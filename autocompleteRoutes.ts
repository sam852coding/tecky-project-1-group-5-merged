import express, { Request, Response, NextFunction } from 'express';
import { client } from './main';

export const autocompleteRoutes = express.Router();



// (GET) 預先讀晒所有名 Search Autocomplete
autocompleteRoutes.get('/recordNames', loadAllNames)

async function loadAllNames(req:Request, res:Response, next: NextFunction) {
    // get all patients name , should be only based on the search query , limit only a few items
    const names = (await client.query(/* sql */`SELECT patients.name from patients`)).rows
    const nameArr = [];
    for (let name of names) {
        nameArr.push(name.name);
    }
    res.json(nameArr);
}
