import express, { Request, Response } from 'express';
import { client } from './main';

export const medicineSearchRoutes = express.Router();



// (GET) 尋找中藥
medicineSearchRoutes.get('/searches', searchMedicine); 

async function searchMedicine (req:Request, res:Response) {
    const {medicine,types} = req.query;
    console.log(req.query )

    const result = await client.query(/*sql*/`SELECT * 
    FROM medicines WHERE med_name ILIKE $1 or type ILIKE $2  `,['%'+medicine+'%','%'+types+'%']); // medicines個table改名問題？ name vs med_name ?
    //WHERE name LIKE $1 `,["%百%"]//
    //用作 call sql db 的東西//
    //下一步會去左express ()//
    //result.rows 係 sql 領落黎//
    // 下一步會轉化成 http 位置 /searchs?//
    // console.log("THIS IS RESULT.ROWS:")
    console.log(result.rows)
   res.json(result.rows);
};