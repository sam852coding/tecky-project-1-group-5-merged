import express, { Request, Response } from 'express';
import { client } from './main';

export const bookingRoutes = express.Router();

bookingRoutes.get('/bookings/:date', searchBookings);

async function searchBookings (req:Request, res:Response) {

    // const { date } = req.params; // TODO
    // console.log("REQ.PARAMS:")
    // console.log(req.params)

    const date = new Date (req.params.date);

    const results = (await client.query(/* sql */`
    SELECT 
    bookings.id as booking_id,
    bookings.date_of_appointment as booking_date,
    bookings.time_of_appointment_start as booking_startTime,
    bookings.time_of_appointment_end as booking_endTime,
    bookings.is_first_visit as is_first_visit,
    patients.name as patient_name, 
    patients.date_of_birth as patient_dob,
    patients.hkid_no as patient_hkid, 
    patients.phone_no as patient_phone, 
    patients.sex as patient_sex
    FROM bookings JOIN patients ON patients.id = bookings.patient_id
    WHERE bookings.date_of_appointment = $1
    ORDER BY booking_date DESC, booking_startTime`,[date])).rows;

    console.log(results);

    res.json(results);
}

// 2020-05-23T16:00:00.000Z
// 2020-05-24T16:00:00.000Z
// 2020-05-25T00:00:00.000Z