
// (GET) 當按下尋找時會發生的事
document.querySelector('#patient-search')
    .addEventListener('submit', async function (event) {
        event.preventDefault(); // 防止個DOM object做咗原來既野 --> instead, submit with AJAX

        const formObject = await loadRecords();
        const records= await searchRecords(formObject);
        await displayRecordInModal(records);
    });


// 用張form的input做query search, 讀取SQL DB的data
async function loadRecords(){
    const form = document.querySelector('#patient-search');

    const formObject = {
        patientName: form.patientName.value,
        patientHkid: form.patientHkid.value,
        patientPhone: form.patientPhone.value,
    }
    console.log("FORM OBJECT IS:");
    console.log(formObject);
    return formObject;
    // await searchRecords(formObject);
}    


// 列出 *所有* 屬於該病人的記錄 
async function searchRecords(formObj) {
    //records/?name=${formObj.patientName}&hkid_no=${formObj.patientHkid}&phone_no=${formObj.patientPhone}

    let fetchURL = "/records/?" // 駁長佢

    if (formObj.patientName !== "") {
        fetchURL += `&name=${formObj.patientName}`;
    }
    if (formObj.patientHkid !== "") {
        fetchURL += `&hkid=${formObj.patientHkid}`;
    }
    if (formObj.patientPhone !== "") {
        fetchURL += `&phone=${formObj.patientPhone}`;
    }
    if (formObj.patientName === "" && formObj.patientHkid === "" && formObj.patientPhone === "") {
        alert("Your search is empty, please try again!");
    }
    console.log("fetchURL is:");
    console.log(fetchURL);

    const res = await fetch(fetchURL, {
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        },
    });

    //records係由database fetch下來的object array，入面有該病人所有紀錄
    const records = await res.json();

    // TODO: Warn user when search is invalid.
    // if (records.length === 0){
    //     Document.getElementById('invalid-search')
    //         .classList.remove('d-none');
    // }

    const recordContainer = document.querySelector('.searched-records');
    recordContainer.innerHTML = ""; //每次先清空wall先再一次過加返data上去

    // Loop個res.json然後display records in table form
    for (let record of records) {
        recordContainer.innerHTML += `
            <tr record-id="${record.record_id}" class="searched-records-row" 
                data-toggle="modal" data-target="#patientRecordModal">
                <td>${record.patient_name}</td>
                <td>${record.record_doc.slice(0, 10)}</td>
                <td>${record.record_diagnosis}</td>
            </tr>
            `
    }
    return records;
    // await displayRecordInModal(records);
}

        // 當click嗰行時會load嗰個record id既病歷落modal table
        async function displayRecordInModal(recordsFromDB) {

            console.log("RECORDS FROM DB:");
            console.log(recordsFromDB);

            // 當你click嗰行data時會發生的事情
            const recordRows = document.querySelectorAll('.searched-records-row')
            for (let recordRow of recordRows) {

                recordRow.onclick = async function (event) {

                    let id = event.target.parentElement.getAttribute('record-id');

                    // 對返id，出病歷
                    for (let recordFromDB of recordsFromDB) {
                        if (id == recordFromDB.record_id) {
                            document.getElementById('record-consultationDate').innerHTML = recordFromDB.record_doc.slice(0, 10);
                            document.getElementById('record-name').innerHTML = recordFromDB.patient_name;
                            document.getElementById('record-age').innerHTML = calculateAge(recordFromDB.patient_dob);
                            document.getElementById('record-sex').innerHTML = recordFromDB.patient_sex;
                            document.getElementById('record-phone').innerHTML = recordFromDB.patient_phone;
                            document.getElementById('record-hkid').innerHTML = recordFromDB.patient_hkid;
                            document.getElementById('record-diagnosis').innerHTML = recordFromDB.record_diagnosis;
                            document.getElementById('record-id').value = id;
                            document.getElementById('record-complaint').value = recordFromDB.record_complaint;
                            // 讀取每一次睇病的藥方
                            document.getElementById('record-medicine').innerHTML = "";
                            for (let prescription of recordFromDB.prescriptions) {
                                document.getElementById('record-medicine').innerHTML += `
                                <tr class="record-medicine-prescription">
                                    <td>${prescription.medicine_name}</td>
                                    <td>${prescription.prescription_quantity}</td>
                                    <td>${prescription.prescription_unit}</td>
                                </tr>
                                `
                            }

                        }
                    }
                }
            }
        }


// Auto-Complete: 先load所有病人名落name search form
async function autocompleteFill() {

    const res = await fetch('/recordNames', {
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        },
    });
    const names = await res.json();
    // Search Autocomplete codes ...
    const options = {
        data: names,
        list: {
            match: {
                // maxNumberOfElements: 8,
                enabled: true
            }
        }
    };
    $("#basics").easyAutocomplete(options);
}
autocompleteFill();



// When modal closed
$('#patientRecordModal').on('hidden.bs.modal', function () {
    console.log("MODAL IS CLOSED!!")
});


// (PUT) 當按下儲存時發生既事
// 改動病歷紀錄後儲存在Database
document.getElementById('save-record')
    .addEventListener('click', async function (event) {
    const id = document.getElementById('record-id').value

    const res = await fetch(`/records/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            complaint: document.getElementById('record-complaint').value
        })
    })

    const result = await res.json();

    loadRecords();
})



// Create PDF using pdfmake.js

const docDefinition = {
	content: [
		{
            text: '你好 Consultation Record)\n',
			style: 'header'
        },
        {text: '\n\nRecords', style: 'header'},
        {
			ul: [
				'patient name',
				'age',
                'gender',
                'diagnosis',
			]
        },
        {text: '\n\nChinese Medicines', style: 'header'},
        {
			ol: [
				'medicine 1',
				'medicine 2',
                'medicine 3',
                'medicine 4',
                'medicine 5',
                '...'
			]
		},
    ],
    styles: {
		header: {
			bold: true,
			fontSize: 15
		}
	},
	defaultStyle: {
        // font: 'fangzhen',
        fontSize: 12
	}	
}

document.getElementById('print-record')
    .addEventListener('click', async () => {

        pdfMake.fonts = { 
            Roboto: { 
                normal: 'Roboto-Regular.ttf', 
                bold: 'Roboto-Medium.ttf', 
                italics: 'Roboto-Italic.ttf', 
                bolditalics: 'Roboto-MediumItalic.ttf' 
            },
            msyh: { 
                normal: 'msyh.ttf', 
                bold: 'msyh.ttf', 
                italics: 'msyh.ttf', 
                bolditalics: 'msyh.ttf',
            }
        };
    await pdfMake.createPdf(docDefinition).open();
})