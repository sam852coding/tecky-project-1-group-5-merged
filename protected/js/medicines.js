document.querySelector('#medicine-search-form')
  .addEventListener('submit', async function (event) {
    event.preventDefault();

    const form = event.target;
    console.log(form)

    const formObject = {
      medicineName: form.medicineName.value,
      medicinetype: form.medicinetype.value,
    }

    await listOfmedicine(form);
  });

async function listOfmedicine(form) {

  let URL = "/searches?"

  if (form.medicineName.value !== "") {
    URL += `&medicine=${form.medicineName.value}`
  }
  if (form.medicinetype.value !== "")
    URL += `&types=${form.medicinetype.value}`

  const res = await fetch(URL, {
    method: "GET"
  })

  const medicines = await res.json();

  const medicinesearch = document.querySelector('.search-table')
  medicinesearch.innerHTML = "";

  for (let medicine of medicines) {
    medicinesearch.innerHTML += `

        <div class="card mb-3 col-sm-6" id="mbbb-3">

          <div class="row no-gutters" id="rownogutters">

            <div class="col-md-4">
            <img src="./upload/${medicine.image}" class="img-fluid"></img>
            </div>
            
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title" ">${medicine.med_name}</h5>
                <p class="card-text" >${medicine.info}</p>
                
              </div>
            </div>
          </div>
        </div> 
        `
  }
}
