document.querySelector('#find-patient-form')
    .addEventListener('submit', async function (event) {
        event.preventDefault(); 
        const form = event.target; 
        console.log(form);

        const formObject = {
            patientName: form.patientName.value,
            patientSex: form.patientSex.value,
            patientDOB: form.patientDOB.value,
            patientHkid: form.patientHkid.value,
            patientPhone: form.patientPhone.value,
        }
        console.log(formObject)

        
        searchPatients(formObject);
    });

async function searchPatients (formObj){
    
    let fetchURL = "/patient/?"
    
    if (formObj.patientName !== ""){
        fetchURL+= `&patientName=${formObj.patientName}`;
    }
    if (formObj.patientHkid !== ""){
        fetchURL+= `&patientHkid=${formObj.patientHkid}`
    }
    if (formObj.patientSex !== ""){
        fetchURL+= `&patientSex=${formObj.patientSex}`
    }
    if (formObj.patientPhone !== ""){
        fetchURL+= `&patientPhone=${formObj.patientPhone}`
    }
    if (formObj.patientDOB !== ""){
        fetchURL+= `&patientDOB=${formObj.patientDOB}`
    }

    
    console.log(`fetchURL is ${fetchURL}`)

    const res = await fetch(fetchURL);
    const patients = await res.json(); 
    console.log(patients);

    const patientContainer = document.querySelector('#patientResultTable');
    patientContainer.innerHTML = ""; //每次先清空wall先再一次過加返data上去

    for (let patient of patients) {
            patientContainer.innerHTML += `
            <tr class="patient-row searchPatientTr">
                <td class="searchPatientTd">${patient.id}</td>
                <td class="searchPatientTd">${patient.name}</td>
                <td class="searchPatientTd">${patient.sex}</td>
                <td class="searchPatientTd">${patient.date_of_birth.slice(0,10)}</td>
                <td class="searchPatientTd">${patient.phone_no}</td>
                <td class="searchPatientTd">${patient.hkid_no}</td>
                <td>
                <a href="/new-records-post.html?patientId=${patient.id}" >
                <button type="button" class="btn" id="patientSelectButton"  data-patient-id="${patient.id}">新增病歷</button>
                </a>
                </td>
            </tr>
            `
    }

}

// function newRecord(patient){
//     // console.log("newRecord is clicked");
//     let patientId = patient.getAttribute("data-patient-id");
//     console.log(`patient id is ${patientId}`);

//     redirectPage(`./createRecord.html?patientId=${patient.id}`);
// }



// function redirectPage(URL){
//     window.location.replace(URL);
//     // window.onload = function() {
//     //     document.querySelector('show-patient-id').innerHTML += patient.id;
//     // }
// }


// function overlayOn() {
//     document.getElementById("overlay").style.display = "block";
// }

// function overlayOff() {
//     document.getElementById("overlay").style.display = "none";
// }



