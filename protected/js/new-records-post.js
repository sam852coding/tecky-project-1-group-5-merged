// let patientId = document.URL.slice(54);
const urlParams = new URLSearchParams(window.location.search);
const patientId = urlParams.get('patientId');
console.log(patientId);

showPatientInfo();

async function showPatientInfo (){

    const res = await fetch(`/patient/${patientId}`,{
        method:"GET",
        headers: {"Content-Type":"application/json"}
    });
    const patient = await res.json(); 
    console.log(patient);

    const patientContainer = document.querySelector('#show-patient-info');
    patientContainer.innerHTML = ""; //每次先清空wall先再一次過加返data上去

    for(let data of patient){
        patientContainer.innerHTML += `
    
        <div class "container-fluid show-patient-block" style="background-color: rgba(28, 49, 58, 0.7);color: #FFFFFF;padding: 5%;">
            <h5 style="font-size: 1rem;"><u>病人編號 :</u><br/>${data.id}</h5>
            <h5 style="font-size: 1rem;"><u>病人姓名 :</u><br/>${data.name}</h5>
            <h5 style="font-size: 1rem;"><u>病人性別 :</u><br/>${data.sex}</h5>
            <h5 style="font-size: 1rem;"><u>出生日期 :</u><br/>${data.date_of_birth.slice(0,10)}</h5>
            <h5 style="font-size: 1rem;"><u>電話號碼 :</u><br/>${data.phone_no}</h5>
            <h5 style="font-size: 1rem;"><u>身份證號碼 :</u><br/>${data.hkid_no}</h5>
        </div>
            `
    }
};

async function autocompleteFill() {

    const res = await fetch('/new-records-post-auto', {
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        }
    });
    const names = await res.json();
    console.log(JSON.stringify(names));
    // Search Autocomplete codes ...
    const options = {
        data: names,
        list: {
            match: {
                enabled: true
            }
        }
    };
    $(".basics").easyAutocomplete(options);
}
autocompleteFill();

document.querySelector('#record-submit-form')
.addEventListener('submit',async (event)=>{
    event.preventDefault(); // 防止個DOM object做咗原來既野
    // (in this case: submit form data to server, refresh page)

    // Serialize the Form afterwards
    const form = event.target; //指個DOM object (<form></form>)
    console.log(form)

    // formObject is only a name, no need for FormData
    const formObject = {};
    formObject['patientDOC'] = form.patientDOC.value; 
    formObject['patientComplaint'] = form.patientComplaint.value; 
    formObject['patientDiagnosis'] = form.patientDiagnosis.value;
    formObject['prescriptionList'] = []; 
    // formObject['patientId'] = patientId;

    const elements = document.querySelectorAll('.prescription-row');

    for (let element of elements){
        let elementObj = {};
        elementObj.prescriptionMedicine = element.querySelector('.prescription-medicine').value;    
        elementObj.prescriptionQty = element.querySelector('.prescription-qty').value;
        elementObj.prescriptionUnit = element.querySelector('.prescription-unit').value;

        if(elementObj.prescriptionMedicine!=='' && elementObj.prescriptionQty !== ''){
        formObject.prescriptionList.push(elementObj);
        }
    }

    console.log(formObject);

    const res = await fetch(`/new-records-post/?patientId=${patientId}`,{
        method:"POST",
        body:JSON.stringify(formObject),
        headers:{
            "Content-Type": "application/json"
        }
    }); 

    setTimeout(Redirect(),2000);
})


document.querySelector("#add-new-row")
.addEventListener('click', async function(event){
    event.preventDefault();
    const newTableRowContainer = document.querySelector('#prescriptionAddTable');
    newTableRowContainer.innerHTML+=`
    <tr class="prescription-row">
    <td>
      <input type="text" class="form-control input-sm prescription-medicine basics" name="prescriptionMedicine" >
    </td>
    <td>
      <input type="text" class="form-control input-sm prescription-qty" name="prescriptionQty">
    </td>
    <td>
      <input type="text" class="form-control input-sm prescription-unit" name="prescriptionUnit">
    </td>
    </tr>
    `
    autocompleteFill();
});

function Redirect() {
    window.alert("成功新增紀錄及處方！");
    window.location.href = './new-records.html';
 }        