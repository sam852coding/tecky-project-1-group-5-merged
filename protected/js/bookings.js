console.log("HELLO WORLD");

document.querySelector('#booking-search')
    .addEventListener('submit', async function (event) {
        event.preventDefault();

        await loadBookings();
    });


async function loadBookings(){
    const form = document.querySelector('#booking-search');

    const formObject = {
        bookingDate: form.bookingDate.value
    }

    console.log("FORM OBJECT IS:");
    console.log(formObject);

    await displayBookings(formObject);
}


async function displayBookings(formObj){

    const fetchURL = `/bookings/${formObj.bookingDate}`
    console.log(fetchURL)
    const res = await fetch (fetchURL,{
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        },
    })

    const bookings = await res.json();
    console.log("response from server is:")
    console.log(bookings);


    const recordContainer = document.querySelector('.searched-booking-records');
        recordContainer.innerHTML = "";

    for (let i = 0; i < bookings.length; i++) {
        recordContainer.innerHTML += `
            <tr booking-id="${bookings[i].booking_id}" class="searched-records-row card-header" id="heading${i}"
                data-toggle="collapse" data-target="#collapse${i}" aria-expanded="false" aria-controls="collapse${i}">
                <td>${bookings[i].booking_starttime.slice(0,5)}</td>
                <td>${bookings[i].patient_name}</td>
                <td>${bookings[i].is_first_visit? '是':'否'}</td>
            </tr>
            <tr id="collapse${i}" class="collapse hide" aria-labelledby="heading${i}" data-parent="#bookings-accordion">
                <td class="card-body extended-booking-content" colspan="3">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <h5>病人詳細資料</h5>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-6">
                                性別：<h6>${bookings[i].patient_sex}</h6>
                                出生日期：<h6>${bookings[i].patient_dob.slice(0,10)}</h6>
                                年齡：<h6>${calculateAge(bookings[i].patient_dob)}</h6>
                            </div>
                            <div class="col-6">
                                電話號碼：<h6>${bookings[i].patient_phone}</h6>
                                身份證號碼：<h6>${bookings[i].patient_hkid}</h6>
                            </div>
                        </div> 
                    </div>
                </td>
            </tr>
        `
    }
        
    
}







// Datepicker
window.onload = ()=>{

    // INITIALIZE DATEPICKER PLUGIN
    $('.datepicker').datepicker({
        clearBtn: true,
        format: "yyyy-mm-dd"
    });

    $('#reservationDate').on('change', function () {
        var pickedDate = $('input').val();
        $('#pickedDate').html(pickedDate);
    });
}