import express, { Request, Response, NextFunction } from 'express';
import { client } from './main';

export const newRecordRoutes = express.Router();



// (GET) 尋找病人：是不是新症？
newRecordRoutes.get('/patient', searchPatients);

async function searchPatients(req:Request, res:Response){
    
    const {patientName,patientPhone, patientSex, patientDOB, patientHkid} = req.query;
    // console.log(`Patient's name is ${patientName}, phone is ${patientPhone}, Sex is ${patientSex}`);    
    // console.log(`DOB: ${patientDOB}`);

    // cleanedPatientName
    // let patientName__ = patientName ?? "";  new way to write in TS 3.7
    let patientName_ = patientName ?  patientName.toString().toUpperCase() :"";
    let patientPhone_ = patientPhone == undefined ? '': patientPhone.toString();
    let patientSex_ = patientSex == undefined ? '': patientSex.toString().toUpperCase();
    // let patientDOB_ = patientDOB == undefined ? '': patientDOB;
    let patientHkid_ = patientHkid == undefined ? '': patientHkid.toString().toUpperCase();

     
    if(!patientDOB){
        const result = await client.query(`SELECT * FROM patients 
        where UPPER(patients.name) like $1 
        and patients.phone_no like $2 
        and patients.sex like $3 
        and patients.hkid_no like $4
        order by patients.id;`,['%'+patientName_+'%','%'+patientPhone_+'%','%'+patientSex_+'%', '%'+patientHkid_+'%']);

        res.json(result.rows);
    }
    else{
        const result = await client.query(`SELECT * FROM patients 
        where UPPER(patients.name) like $1 
        and patients.phone_no like $2 
        and patients.sex like $3 
        and patients.hkid_no like $4
        and patients.date_of_birth = $5
        order by patients.id;`,['%'+patientName_+'%','%'+patientPhone_+'%','%'+patientSex_+'%', '%'+patientHkid_+'%',patientDOB]);

        res.json(result.rows);
    }
}


// (GET) 當新增病歷時可以確認是否選擇了正確的病人
newRecordRoutes.get('/patient/:patientId', showPatientInfo)

async function showPatientInfo(req:Request, res:Response){
    const {patientId} = req.query;
    console.log(`Patient's id is ${patientId}`);    
    const result = await client.query (`SELECT * FROM patients where patients.id = ${patientId}`)
    res.json(result.rows);
}

// Should be called /medicine-name
newRecordRoutes.get('/new-records-post-auto', loadAllMedicines)

async function loadAllMedicines(req:Request, res:Response, next: NextFunction) {
    const names = (await client.query(`SELECT med_name from medicines`)).rows
    const nameArr = [];
    for (let name of names) {
        nameArr.push(name.med_name);
    }
    res.json(nameArr);
}


// should be called /records
// (POST) 把新增的處方及病歷一同傳送到SQL
newRecordRoutes.post('/new-records-post', postRecords)

async function postRecords(req:Request, res:Response){
    // patientId should be inside body
    const {patientId}=req.query;
    console.log("REQ.BODY: ");
    console.log(req.body);
    const patientDOC = req.body.patientDOC;
    const patientComplaint = req.body.patientComplaint;
    const patientDiagnosis = req.body.patientDiagnosis;
    const prescriptionList = req.body.prescriptionList;

    // console.log(`patientDOC is ${patientDOC}. patientComplaint is ${patientComplaint}. patientDiagnosis is ${patientDiagnosis}. prescriptionList is ${prescriptionList}. patientID is ${patientId} . `);    
    // console.log(`prescriptionMedicine is ${prescriptionList[0].prescriptionMedicine}`)
    
    const result_result = await client.query(`INSERT INTO records 
             (date_of_consultation, complaint ,diagnosis, patient_id ) 
             VALUES ($1, $2, $3, $4) 
             RETURNING id
         `, [patientDOC, patientComplaint, patientDiagnosis, patientId]);

    let result_id = result_result.rows[0].id;

    for(let prescription of prescriptionList){
        let medicineName = prescription.prescriptionMedicine;
        // console.log(medicineName);
        let medicineId = await client.query(`SELECT id FROM medicines where med_name = '${medicineName}';`)
        prescription.medicineId = medicineId.rows[0].id;
        prescription.resultId = result_id;
        console.log(prescription);
        // console.log(`Medicine id is ${prescription.medicineId}`);
        await client.query (`INSERT INTO prescriptions (quantity, unit, record_id, medicine_id)
        VALUES ($1, $2, $3, $4)`, 
        [prescription.prescriptionQty, prescription.prescriptionUnit, prescription.resultId, prescription.medicineId]);
    } 

    res.json({success:true});
}