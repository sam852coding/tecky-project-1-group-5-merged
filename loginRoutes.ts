import express, { Request, Response } from 'express';
import { client } from './main';
import { checkPassword } from './hash';
import {User} from './login'

export const loginRoutes = express.Router();



loginRoutes.post('/login', postLoginInfo);

async function postLoginInfo(req: Request, res: Response) {
    const { username, password } = req.body;
    const result = await client.query(/*sql*/`SELECT * from users where username = $1`, [username]);
    const users: User[] = result.rows;
    const user = users[0];
    if (user && await checkPassword(password, user.password)) {
        if (req.session) {
            req.session.user = user; // 係個session 度mark 咗你係成功login 咗
        }
        res.redirect('/search-records.html');
    } else {
        //login 失敗
        res.status(401).redirect("/?error=login+failed")
    }
}

