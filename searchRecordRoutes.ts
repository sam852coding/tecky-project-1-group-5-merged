import express, { Request, Response } from 'express';
import { client } from './main';

export const searchRecordRoutes = express.Router();



// (GET) 尋找病歷
searchRecordRoutes.get('/records', searchRecords);

async function searchRecords (req:Request, res:Response) {

    const { name, hkid, phone } = req.query;

    // console.log("REQ.QUERY:")
    // console.log(req.query)

    const originalResults = (await client.query(/* sql */`
    SELECT 
    records.id as record_id, 
    records.date_of_consultation as record_doc, 
    records.complaint as record_complaint, 
    records.diagnosis as record_diagnosis,
    patients.name as patient_name, 
    patients.date_of_birth as patient_dob,
    patients.hkid_no as patient_hkid, 
    patients.phone_no as patient_phone, 
    patients.sex as patient_sex,
    prescriptions.quantity as prescription_quantity, 
    prescriptions.unit as prescription_unit, 
    medicines.med_name as medicine_name
    FROM records 
    JOIN patients ON patients.id = records.patient_id 
    JOIN prescriptions ON records.id = prescriptions.record_id
    JOIN medicines ON medicines.id = prescriptions.medicine_id
    WHERE   patients.name LIKE $1 OR 
            patients.hkid_no LIKE $2 OR 
            patients.phone_no LIKE $3
    ORDER BY date_of_consultation DESC;`,
        ['%' + name + '%', '%' + hkid + '%', phone])).rows;

        

    // console.log("THIS IS ORIGINAL RESULTS:");
    // console.log(originalResults);

    let groupedRecords = {};
    for (let item of originalResults) {
        const { prescription_quantity, prescription_unit, medicine_name } = item;
        const prescription = { prescription_quantity, prescription_unit, medicine_name }
        if (groupedRecords[item.record_id]) {
            groupedRecords[item.record_id].prescriptions.push(prescription);
        } else {
            groupedRecords[item.record_id] = { ...item, prescriptions: [prescription] };
        }
    }
    const results = Object.values(groupedRecords);

    // console.log("RESULTS JSON SEND TO FROUNT-END:")
    // console.log(results)
    res.json(results);
}


// (PUT) 更改病歷
searchRecordRoutes.put('/records/:id', editRecords)

async function editRecords (req:Request, res:Response) {
    
    
    try {
        const recordId = parseInt(req.params.id);
        const { complaint } = req.body;
        await client.query (/*sql*/`UPDATE records 
        SET complaint = $1 
        WHERE id = $2`, [complaint, recordId]);
        res.json({ success: true })
    } catch(err){
        console.log(err.message)
        res.status(400).json({msg:err});
    }

}