/* Migrations */

CREATE TABLE users(
     id SERIAL,
     username TEXT,
     password varchar(255)
);

CREATE TABLE bookings(
    id SERIAL PRIMARY KEY,
    date_of_appointment DATE not null,
    time_of_appointment_start TIME not null,
    time_of_appointment_end TIME not null,
    is_first_visit BOOLEAN,
    patient_id INTEGER,
    FOREIGN KEY (patient_id) REFERENCES patients(id)
);

CREATE TABLE patients(
    id SERIAL PRIMARY KEY,
    name varchar(255) not null,
    date_of_birth DATE not null,
    hkid_no VARCHAR(20),
    phone_no VARCHAR(255),
    sex varchar(10) not null,
    created_at TIMESTAMP not null,
    updated_at TIMESTAMP not null
);

CREATE TABLE records(
    id SERIAL PRIMARY KEY,
    date_of_consultation DATE not null,
    complaint TEXT not null,
    diagnosis TEXT not null,
    patient_id INTEGER,
    FOREIGN KEY (patient_id) REFERENCES patients(id)
)

    -- western_med TEXT not,
    -- chinese_med TEXT,
    -- checkup TEXT,
    -- acupunture TEXT,
    -- symptoms TEXT,
    -- take_method TEXT,
    -- fee INTEGER not null,
    -- staff varchar(255),

CREATE TABLE medicines(
    id SERIAL PRIMARY KEY,
    image TEXT,
    name VARCHAR(255) not null,
    type VARCHAR(255) not null,
    info TEXT not null
);

CREATE TABLE prescriptions(
    id SERIAl PRIMARY KEY,
    quantity decimal(5,2) not null,
    unit varchar(10) not null,
    record_id INTEGER,
    FOREIGN KEY (record_id) REFERENCES records(id),
    medicine_id INTEGER,
    FOREIGN KEY (medicine_id) REFERENCES medicines(id)
)


--- Actions Below

select * from patients;


/* Seed Data */

INSERT INTO patients (name, date_of_birth, hkid_no, phone_no, sex, created_at, updated_at) VALUES 
('何君堯', '1962-06-04', 'A123456(7)', '99998888', '男', NOW(), NOW()),
('葉劉淑儀', '1950-08-24', 'B234567(8)', '99997777', '女', NOW(), NOW());
('鄧炳強', '1965-07-04', 'A123456(7)', '99998888', '男', NOW(), NOW()),
('鄭若驊', '1958-11-11', 'Y546841(8)', '9999666', '女', NOW(), NOW());
('林鄭月娥', '1957-05-13', 'J554684(8)', '9999555', '女', NOW(), NOW());
('梁振英', '1954-08-12', 'A123456(7)', '99998888', '男', NOW(), NOW()),
('唐青儀', '1957-02-05', 'J554684(8)', '9999555', '女', NOW(), NOW());
('梁齊昕', '1991-09-11', 'J554684(8)', '9999555', '女', NOW(), NOW());

INSERT INTO bookings (date_of_appointment, time_of_appointment_start, time_of_appointment_end, is_first_visit, patient_id) VALUES 
('2020-05-24', '11:00 AM', '11:15 AM', FALSE, 1),
('2020-05-23', '09:30 AM', '09:45 AM', TRUE, 2),
('2020-05-23', '04:00 PM', '04:30 PM', FALSE, 3),
('2020-05-24', '5:00 PM', '05:15 PM', FALSE, 2),
('2020-05-25', '02:30 PM', '02:45 PM', FALSE, 1),
('2020-05-25', '10:30 AM', '10:45 AM', FALSE, 3);

('2020-05-24', '11:21 AM', '11:15 AM', FALSE, 1),
('2020-05-23', '09:30 AM', '09:45 AM', TRUE, 2),
('2020-05-23', '04:00 PM', '04:21 PM', FALSE, 3),
('2020-05-24', '5:00 PM', '05:21 PM', FALSE, 2),
('2020-05-25', '02:30 PM', '02:49 PM', FALSE, 1),
('2020-05-25', '10:30 AM', '10:37 AM', FALSE, 3);

('2020-05-24', '12:00 AM', '03:15 AM', FALSE, 1),
('2020-05-23', '04:30 AM', '02:25 AM', TRUE, 2),
('2020-05-23', '03:00 PM', '05:21 PM', FALSE, 3),
('2020-05-24', '5:00 PM', '08:29 PM', FALSE, 2),
('2020-05-25', '01:30 PM', '03:34 PM', FALSE, 1),
('2020-05-25', '09:30 AM', '12:47 AM', FALSE, 3);

INSERT INTO records (date_of_consultation, complaint, diagnosis) VALUES 
('2020-05-14', '頭痛，頸痛，精神萎靡。', '氣虛血弱'),
('2020-04-26', '失眠，盗汗，手震。', '氣虛血弱'),
('2020-03-08', '食慾不振，手腳冰冷。', '陰虛火旺'),
('2019-12-24', '精神錯亂，心理變態。', '肝鬱氣滯'),

('2020-03-17', '暈眩，頭重，嘔吐。', '高血壓'),
('2020-02-28', '浮腫，流鼻水，咳嗽。', '心肌炎'),
('2020-09-18', '易餓，疲倦。', '糖尿病'),
('2019-06-12', '酸痛，腫脹', '下肢靜脈曲張'),



INSERT INTO medicines (image, name, type, info) VALUES 
('t01448ffec65ffda5fb.jpg','百合','葉類','本品為百合科多年生草本植物百合 (Lilium brownii F. E. Brown var. viridulum Baker) 或細葉百合 (L. pumilum DC.) 的肉質鱗葉。全國各地均產，以湖南，浙江產者為多。秋季採挖，洗淨，剝取鱗葉，置沸水中略燙，乾燥。生用或蜜炙用。'),
('4ns100039s9n153255r8.jpg','側柏葉','葉類','本品為柏科常綠喬木植物側柏 (Platyclatus orientalis (L.) Franco) 的嫩枝葉。全國各地均產。全年可採。陰乾，切段。生用或炒炭用。'),
('569100049npr418sq333.jpg','大青葉','葉類','本品為十字花科二年生草本植物菘藍 (Isatis indigodica Fort.) 的葉片。主產於江蘇、安徽河北、河南、浙江等地。冬季栽培，夏秋採收。鮮用或曬乾生用。');

('46sn00050np8pn7r53p5red.jpg','紅花','花類','本品為菊科一年生草本植物紅花 (Carthamus tinctorius L.) 的花。全國各地多有栽培，主產於河南、浙江、四川，江蘇等地。夏季花由黃變紅時採摘，陰乾或曬乾，生用。'),
('3046325257576251434agold.jpg','金銀花','花類','本品為忍冬科多年生半常綠纏繞性木質藤本植物忍冬 (Lonicera japonica Thunb.) 的花蕾。我國南北各地均有分佈。夏初當花含苞未放時採摘，陰乾。生用，炒用或製成露劑使用。'),
('5270007ro16n49p6920win.jpg','款冬花','花類','本品為菊科多年生草本植物款冬 (Tussilago farfara L.) 的花蕾。主產於河南、甘肅、山西、陝西等地。12月或地凍前當花尚未出土時採挖，除去花梗，陰乾，生用，或蜜灸用。'),

('304b71324c76665a4173sin.jpg','地骨皮','皮類','本品為茄科落葉灌木植物枸杞 (Lycium chinensis Mill.) 或寧夏枸杞 (Lycium barbarum L.) 的根皮。分佈於我國南北各地。初春或秋後採挖，剝取根皮，曬乾，切段入藥。'),
('lqkjelfkjlkjlqflkj.jpg','杜仲','皮類','本品為杜仲科落葉喬木植物杜仲 (Eucommia ulmoides Oliv.) 的樹皮。主產四川、雲南、貴州、湖北等地。4～6月剝取，刮去粗皮，堆置「發汗」至內皮呈紫褐色，曬乾。切塊或絲，生用或鹽水炙用。'),
('3049737033654b666f73yellow.jpg','黃柏','皮類','本品為芸香科落葉喬木植物黃蘗（關黃柏） (Phelloendron amurense Rupr.) 黃皮樹 (川黃柏) (Phellodron chinense Schneld.) 除去栓皮的樹皮。關黃柏主產於遼寧、吉林、河北等地；川黃柏主產於四川、貴州、湖北、雲南等地。清明前後，剝取樹皮，刮去粗皮，曬乾壓平，潤透切片或切絲，生用或鹽水炙、酒炙、炒炭用。');


UPDATE records SET patient_id = 2 where id = 1;
UPDATE records SET patient_id = 3 where id = 2;
UPDATE records SET patient_id = 1 where id = 3;
UPDATE records SET patient_id = 3 where id = 4;

--- Inner join records and patients tables
SELECT * FROM records join patients on patients.id = records.patient_id;

INSERT INTO prescriptions (quantity, unit, record_id, medicine_id) VALUES
(2, '錢', 1, 2),
(3, '兩', 1, 1),
(0.5, '克', 1, 3),

(2, '錢', 2, 2),
(4, '克', 2, 3),

(2, '兩', 3, 1),
(1, '克', 3, 3),

(2, '錢', 4, 3),
(0.5, '克', 4, 3),

(2, '錢', 5, 1),
(2.5, '克', 5, 3),

(2.5, '錢', 6, 2),
(3, '兩', 6, 1),
(0.5, '克', 6, 3);

SELECT records.id, records.date_of_consultation, records.complaint, records.diagnosis, patients.*
FROM records JOIN patients ON patients.id = records.patient_id
JOIN prescriptions ON records.id = prescriptions.record_id
JOIN medicines ON medicines.id = prescriptions.medicine_id


SELECT 
bookings.id as booking_id,
bookings.date_of_appointment as booking_date,
bookings.time_of_appointment_start as booking_time_start,
bookings.time_of_appointment_end as booking_time_end,
patients.name as patient_name, 
patients.date_of_birth as patient_dob,
patients.hkid_no as patient_hkid, 
patients.phone_no as patient_phone, 
patients.sex as patient_sex
FROM bookings JOIN patients ON patients.id = bookings.patient_id
ORDER BY booking_date DESC, booking_time_start