const searchParams = new URLSearchParams(window.location.search);
const errMessage = searchParams.get('error');

if (errMessage) {
    const alertBox = document.createElement('div');
    alertBox.classList.add('alert', 'alert-danger');
    alertBox.textContent = errMessage;
    document.querySelector('#error-message').appendChild(alertBox);

    setTimeout(async function(){ await document.getElementById("error-message").remove() }, 3000);
}