import express,{Request,Response,NextFunction}from 'express';
import expressSession from 'express-session'
import bodyParser from 'body-parser';
import path from 'path';
import pg from 'pg';
import dotenv from 'dotenv';


dotenv.config();
export const client = new pg.Client({
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: "localhost",
    port: 5432
});
client.connect();

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(expressSession({
    secret: 'Chinese Medicine Clinic System',
    resave: true,
    saveUninitialized: true
}))

/* ------------------- 我是分隔線 ------------------- */



// (阿公) search autocomplete
import {autocompleteRoutes} from './autocompleteRoutes';
app.use('/', autocompleteRoutes);


// (Mo) Login page with hashed password 登入系統
import {loginRoutes} from './loginRoutes'
app.use('/', loginRoutes);


// (Samuel) booking management 預約診症
import {bookingRoutes} from './bookingRoutes'
app.use('/', bookingRoutes);


// (Helen) new-records.html 新增病歷紀錄
import {newRecordRoutes} from './newRecordRoutes';
app.use('/', newRecordRoutes);


// (Samuel) search-records.html 尋找病歷紀錄
import {searchRecordRoutes} from './searchRecordRoutes';
app.use('/', searchRecordRoutes);


// (Mo) 中藥索引
import {medicineSearchRoutes} from './medicineSearchRoutes';
app.use('/', medicineSearchRoutes);



/* ------------------- 我是分隔線 ------------------- */

const isLoggedIn = function(req:Request,res:Response,next:NextFunction){
    // console.log(req.session);

    if(req.session){
        if(req.session.user){
            next();
            return;
        }
    }

    res.redirect('/');
}

app.use(express.static('public'));
app.use(isLoggedIn, express.static('protected'));

app.use((req, res) => {
    res.sendFile(path.join(__dirname, "public/404.html"));
});

const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});